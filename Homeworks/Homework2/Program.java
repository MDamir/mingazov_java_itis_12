import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();

		int b = a % 10;
		int c = a / 10;
		int d0 = b * 10000;
		int e = c % 10;
		int f = c / 10;
		int d1 = e * 1000;
		int g = f % 10;
		int h = f / 10;
		int d2 = g * 100;
		int i = h % 10;
		int j = h / 10;
		int d3 = i * 10;
		int k = j % 10;

		int reversed = (d0 + d1 + d2 + d3 + k);

		System.out.println("reversed: " + reversed);
	}
}