import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int currentNumber = 0;
		int increase = 0;
		int result = 0;
		while (currentNumber != -1) {
			currentNumber = scanner.nextInt();
			if (currentNumber != 0) {
				if (currentNumber > 10) {
					increase = currentNumber % 10 * (currentNumber / 10);
				} else if (currentNumber % 10 == 0) {
					increase = 0;
				} else {
					increase = currentNumber;
				}
			}
			if (increase % 3 == 0 && increase != 0) {
				System.out.println("Произведение цифр которое делится на 3: " + increase);
				result += currentNumber;
			}
		}
		System.out.println("Сумма чисел, произведение цифр которых делится на 3: " + result);
	}
}